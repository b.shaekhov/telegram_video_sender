import threading
import time
import traceback
import parsingSystem.parsingTimer
import postingSystem.postingTimer
import asyncio


updateEvent = []


if __name__ == '__main__':
    updateEvent.append(parsingSystem.parsingTimer.work)
    updateEvent.append(postingSystem.postingTimer.work)

    while True and False:
        for func in updateEvent:
            try:
                func()
            except Exception:
                print(traceback.format_exc())
                
    print('system disabled!')
