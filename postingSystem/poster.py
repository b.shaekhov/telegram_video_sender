import postingSystem.downloader
import postingSystem.poster
import parsingSystem.parser
import DaemonsManager
from telethon import TelegramClient
from telethon.tl.types import DocumentAttributeVideo
from telethon.tl.types import InputPeerUser
import mimetypes
#import sys
import socks
import asyncio
import requests
import time

videoSaveLocation = 'vid.mp4'
thumbSaveLocation = "prewiew.JPEG"

#-------------------Configure client------------------
# Идентификационые данные устарели

entity = 'telegram_session' #имя сессии - все равно какое
api_id = 1146148
api_hash = 'ea80e9e8414f4478f6c713270198dc44'
#proxyIp = "213.32.48.42"
#proxyPort = 52576
#proxy = (socks.SOCKS4, proxyIp, proxyPort)
client = TelegramClient(entity, api_id, api_hash)#,proxy=proxy)
#-----------------------------------------------------------------

async def connectToClient():
    #-----------Connect client------
    await client.connect()
    await client.start()
    #return
    #-------------------------------

async def sendToTG(sendFile, message):# sendFile - путь к видео, который нужно отправить в ТГ. Message - текст, который нужно прикрепить к сообщению:
    
    #loop = asyncio.new_event_loop()
    #asyncio.set_event_loop(loop)
    #postingSystem.poster.client = TelegramClient(entity, api_id, api_hash, loop=loop)#,proxy=proxy)

    await connectToClient()

    print ('connected')

    #client.start()

    file_path = sendFile
    mimetypes.add_type('video/mp4','.mp4')

    contactss = await client.get_dialogs()
    #print(contactss)

    contactss = list(filter(lambda x:x.id == -1001185096913, contactss))
    


    def download_progress_callback(downloaded_bytes, total_bytes):
        print(str(downloaded_bytes) + "     :     " + str(total_bytes));

    msg = await client.send_file(
                            contactss[0].input_entity,
                            file_path,
                            caption=str(message),
                            thumb="prewiew.JPEG",
                            supports_streaming=True,
                            progress_callback=download_progress_callback,
                            attributes=[DocumentAttributeVideo(0,0,0,True,True)]
                            )
    
    

    #print( client._authorized)

    #msg = await client.send_message(
    #                      contactss[0].input_entity,#InputPeerUser("1049991065", "1638074603029678638"),#"VideoTube",
    #                      message="ghfhgfhgfgghfhfgghfhg"
    #                      )

    await client.disconnect()
    #print (msg)
    #input("olol");
    #return 0
    return msg.id

def postVideo(videoRow):
    print ('posting...')

    name = videoRow[parsingSystem.parser.videoNameColumn]
    videoUrl = videoRow[parsingSystem.parser.videoURLColumn]
    thumbUrl = videoRow[parsingSystem.parser.videoThumbUrlColumn]
    #categories = videoRow[parsingSystem.parser.videoCategiriesColumn]
    
    print ('downloading...')
    postingSystem.downloader.download(videoUrl, videoSaveLocation)
    postingSystem.downloader.downloadImage(thumbUrl, thumbSaveLocation)
    
    print ('send to TG...')
    loop = asyncio.get_event_loop()

    ans = loop.run_until_complete(
        sendToTG(videoSaveLocation, name)
    )
    
    print ('posted')

    return ans #telegram postId
    #return 255462
