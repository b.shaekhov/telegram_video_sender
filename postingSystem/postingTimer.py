import time
from MySQLDatabaseTools import MySQLDatabase
import traceback
from settings import *
import parsingSystem.parser
import postingSystem.poster
import postingSystem.postingTimer
from datetime import timedelta, datetime

tableName = 'postedVideos'
videoIdColumn = 'postedId'
tgPostIdColumn = 'tgPostId'
errorColumn = 'error'

sleepTime = 3600
awaitParseSleepTime = 10


awakeTime = datetime.now()



def work():
    
    if awakeTime > datetime.now():
        return

    db =  MySQLDatabase(MySQLAddress, MySQLLogin, MySQLPassword, MySQLdb)

    if not db.tableExist(tableName) :
        db.createTable(tableName, { videoIdColumn : int, tgPostIdColumn : int, errorColumn:str}, pk=primaryKeyColumn)

    parserTable = parsingSystem.parser.tableName

    if not db.tableExist(parserTable):
        print("allVideosPosted! I go asleep")
        postingSystem.postingTimer.awakeTime = datetime.now()+timedelta(seconds=awaitParseSleepTime)
        return

    #exceptQuery = "SELECT * FROM " + parserTable + " WHERE "+primaryKeyColumn+" IN (SELECT "+primaryKeyColumn+" FROM " + parserTable + " EXCEPT SELECT "+primaryKeyColumn+" FROM "+ tableName+") LIMIT 1"
    exceptQuery = "SELECT * FROM " + parserTable + " WHERE "+primaryKeyColumn+" NOT IN (SELECT "+primaryKeyColumn+" FROM "+ tableName+") LIMIT 1"
    queryResult = db.exec( exceptQuery )

    videoRow = db.buildHeaderAndData(queryResult[0], queryResult[1][0])
    
    id = videoRow[primaryKeyColumn]

    try:
        postId = postingSystem.poster.postVideo(videoRow)
    except Exception:
        erText = traceback.format_exc()
        print(erText)
        db.addData(tableName, {videoIdColumn:id, tgPostIdColumn: -1, errorColumn:erText}, pk=primaryKeyColumn)
        postingSystem.postingTimer.awakeTime = datetime.now()+timedelta(seconds=awaitParseSleepTime)
    else:
        print('after posting no error')
        db.addData(tableName, {videoIdColumn:id, tgPostIdColumn:postId, errorColumn:''}, pk=primaryKeyColumn)
        postingSystem.postingTimer.awakeTime = datetime.now()+timedelta(seconds=sleepTime)
