#!/usr/bin/env python
import youtube_dl
import requests
import urllib.parse as urlparse
import os

##################################### CHECKINGS 

def url_check(url):
    parsed = urlparse.urlparse(url)
    if parsed.netloc == "rt.pornhub.com":
        print ("PornHub url validated.")
    else:
        raise Exception("This is not a PornHub url. [" + parsed.netloc + "]")

def alive_check(url):
    request = requests.get(url)
    if request.status_code == 200:
        print ("and the URL is existing.")
    else:
        raise Exception("but the URL does not exist.")
        

##################################### DOWNLOADING

def download(srcUrl, saveLocation):
    url_check(srcUrl)
    alive_check(srcUrl)

    #dlLoc = ""

    #outtmpl = dlLoc + 'vid.mp4'
    if (os.path.exists(saveLocation)):
        os.remove(saveLocation)
    ydl_opts = {
        'format': 'best',
        'outtmpl': saveLocation,
        'nooverwrites': True,
        'no_warnings': False,   
        'ignoreerrors': True,
        'progress_with_newline':True,
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([srcUrl])

def downloadImage(url, saveLocation):
    p = requests.get(url)
    out = open(saveLocation, "wb")
    out.write(p.content)
    out.close()
