import requests
from bs4 import BeautifulSoup
import re

from MySQLDatabaseTools import MySQLDatabase
import traceback
from settings import *

tableName = 'allVideos'
videoURLColumn = 'url'
videoNameColumn = 'name'
videoThumbUrlColumn = 'thumbUrl'
videoCategiriesColumn = 'categories'
errorDubleColumn = 'errorDuble'

def writeToDbNewVideos(videos):

    db =  MySQLDatabase(MySQLAddress, MySQLLogin, MySQLPassword, MySQLdb)

    if not db.tableExist(tableName) :
        db.createTable(tableName, { videoNameColumn : str, videoURLColumn : str, videoThumbUrlColumn : str, videoCategiriesColumn : str, errorDubleColumn:bool}, pk=primaryKeyColumn)

    ans = False

    #------------Get videos from Source---------------
    mainVideos = videos
    partKeys = mainVideos.keys()
    #-------------------------------------------------

    for partKey in partKeys:
        videoNames = mainVideos[partKey].keys()
        for videoName in videoNames:
            
            try:
                name = videoName
                videoUrl = mainVideos[partKey][videoName][0]
                thumbUrl = mainVideos[partKey][videoName][1]

                #dbRows = list(db[tableName].rows_where(videoURLColumn+" = ?", [videoUrl]))

                queryAns = db.getRowsWhere(tableName, videoURLColumn+" = '"+videoUrl+"'")

                dbRows = list(queryAns[1])

                if len(dbRows) == 0 :
                    #db[tableName].insert( {videoNameColumn : name, videoURLColumn : videoUrl, videoThumbUrlColumn : thumbUrl} )
                    db.addData(tableName, { videoNameColumn : name, videoURLColumn : videoUrl, videoThumbUrlColumn : thumbUrl, errorDubleColumn: False}, pk=primaryKeyColumn)
                    ans = True
                #elif len(dbRows) == 1:
                #    buildedRow = db.buildHeaderAndData(queryAns[0],dbRows[0])
                #    if buildedRow[videoNameColumn] != name or buildedRow[videoThumbUrlColumn] != thumbUrl or buildedRow[videoURLColumn] != videoUrl:
                #        print('video params not equals!')
                #        db.addData(tableName, { videoNameColumn : name, videoURLColumn : videoUrl, videoThumbUrlColumn : thumbUrl, errorDubleColumn:True}, pk=primaryKeyColumn)
                else:
                    newRow = True
                    for row in dbRows:
                        row = db.buildHeaderAndData(queryAns[0],row)
                        if row[videoNameColumn] == name and row[videoThumbUrlColumn] == thumbUrl and row[videoURLColumn] == videoUrl:
                            newRow = False
                            break
                    if newRow:
                        print('video params not equals! Video count = '+ str(len(dbRows)))
                        db.addData(tableName, { videoNameColumn : name, videoURLColumn : videoUrl, videoThumbUrlColumn : thumbUrl, errorDubleColumn:True}, pk=primaryKeyColumn)
                    #print('video count = ' + str(len(dbRows)))
            except Exception:
                print(traceback.format_exc())

    return ans#Найдено ли новое видео?

#Удаляет нечитаемые символы с начала и с конца строки
def clearTitles(title):
    return re.sub('[\t\r\n]', '', title).strip()

#Скачивает все ссылки на видео с указанной страницы
#return { 'section' : { 'videoName' : ['video url', 'preview url'] } }
def parse(page): #page - Номер страницы с видео, откуда нужно скачать ссылки
    print ('parsing ' + str(page) + "...")
    
    url = 'https://rt.pornhub.com'
    r = requests.get(url + '/video?page=' + str(page))
    text = r.text

    #--------------------Output html file for tests---------------
    #output_file = open('test.html', 'w', encoding='utf-8')
    #output_file.write(r.text)
    #with  as output_file:
    #-------------------------------------------------------------
    
    #----------------Find sections--------------------------------
    soup = BeautifulSoup(text,features="html.parser")
    sections = soup.find_all('div', {'class': 'sectionWrapper'})
    #-------------------------------------------------------------

    ans = {}

    for section in sections:

        #--------------------------------------Init Section----------------------
        soup = BeautifulSoup(str(section),features="html.parser")
        titlesoup = BeautifulSoup(str(soup.find('div', {'class': 'sectionTitle'})),features="html.parser")
        sectionName = clearTitles(str(titlesoup.find(['h1','h2']).text))
        #------------------------------------------------------------------------

        ans[sectionName] = {}

        #find videos
        sectVideos = soup.findAll('li', {'class' : 'videoBox'})
        for video in sectVideos:
            #----------------------Find preview image for video---------------------------------------
            previewSoup = BeautifulSoup(str(video),features="html.parser").find('a', {'class' : 'linkVideoThumb js-linkVideoThumb img'})
            preview = BeautifulSoup(str(previewSoup),features="html.parser").find('img')['data-thumb_url']
            #--------------------------------------------------------------------------------

            #---------------------Find video title and local URL---------------------
            videoSoup = BeautifulSoup(str(video),features="html.parser")
            title = BeautifulSoup(str(videoSoup.find('span', {'class' : 'title'})), "html.parser").find('a')
            videoURL = title['href']
            #--------------------------------------------------------------------

            #writeOutput
            ans[sectionName][clearTitles(str(title.text))] = [url + videoURL, preview]
            
            #---------------------tests-------------------
            #output_file.write(clearTitles(str(title.text))+'\n')
            #print(sectionName + ' => ' + clearTitles(str(title.text)) + ' => ' + (url + title['href']))
            #print(clearTitles(str(title.text)) + "      ****       " + url + title['href'])
            #-----------------------------------------------

    finalAns = writeToDbNewVideos(ans)
    print ('end parsing')
    return finalAns
