import time
from MySQLDatabaseTools import MySQLDatabase
import traceback
import parsingSystem.parser
import parsingSystem.parsingTimer
from settings import *
from datetime import timedelta, datetime

tableName = 'parsePageQuery'
startPageColumn = 'startPage'
errorColumn = 'error'
sleepTime = 2

awakeTime = datetime.now()

def work():

    if parsingSystem.parsingTimer.awakeTime > datetime.now():
        return

    db =  MySQLDatabase(MySQLAddress, MySQLLogin, MySQLPassword, MySQLdb)

    if not db.tableExist(tableName) :
        db.addData(tableName, { startPageColumn : 1, errorColumn : False }, pk=primaryKeyColumn)

    allRows = db.getAllRows(tableName)
        
    for row in allRows[1]:
        row = db.buildHeaderAndData(allRows[0], row)

        curPage = row[startPageColumn]

        try:
            findNewVideo = parsingSystem.parser.parse(curPage)
        except Exception:

            print(traceback.format_exc())

            if row[errorColumn] :
                curPage +=1
                row[errorColumn] = False
            else:
                row[errorColumn] = True
        else:
            curPage += 1
            if not findNewVideo :
                curPage = 1
            
        row[startPageColumn] = curPage
        db.updData(tableName, row, pk=primaryKeyColumn)

    parsingSystem.parsingTimer.awakeTime = datetime.now()+timedelta(seconds=sleepTime)
