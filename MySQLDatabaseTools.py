import pymysql #require PyMySQL in requirements.txt

class MySQLDatabase:
    def __init__(self, address, user, password, dbName):

        self.conn = pymysql.connect(address, user, password, dbName)

        self.cursor = self.conn.cursor()
        self.dbName = dbName

    def tableExist(self, tableName):
        #print("SHOW TABLES")
        self.cursor.execute("SHOW TABLES")
        allTables = self.cursor.fetchall()

        for tuple in allTables:
            if tuple[0] == tableName:
                return True
        return False

    def createTable(self, tableName, columns, pk):

        query = 'CREATE TABLE `'+self.dbName+'`.`'+tableName+'` ( ' + '`'+pk+'` INT NOT NULL AUTO_INCREMENT '

        for column in columns:

            query += ', `' + column + '` '

            if columns[column] == int:
                query += 'INT'
            elif columns[column] == str:
                query += 'TEXT'
            elif columns[column] == bool:
                query += 'BOOLEAN NOT'
            else:
                raise NotImplementedError('type: ' + str(columns[column]))

            query += ' NULL '

        query +=  ', PRIMARY KEY (`'+pk+'`) ) ENGINE = InnoDB;'

        #print(query)
        self.cursor.execute(query)
        self.conn.commit()

        #CREATE TABLE `video_poster`.`testTable` ( `id` INT NOT NULL AUTO_INCREMENT , `testColumn` TEXT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

    def addData(self, tableName, data, pk):
        if not self.tableExist(tableName):
            columns = {}
            for dataKey in data:
                columns[dataKey] = type(data[dataKey])
            self.createTable(tableName, columns, pk)

        query = 'INSERT INTO `'+tableName+'` ( `' +pk+'` '

        for datakey in data:
            query += ', `'+datakey+'` '

        query += ') VALUES (NULL'

        for dataKey in data:
            if type(data[dataKey]) == bool:
                query += ", '"+str(1 if data[dataKey] else 0)+"' "
            else:
                query += ", '"+pymysql.escape_string(str(data[dataKey]))+"' "

        query += ')'

        #print(query)
        self.cursor.execute(query)
        self.conn.commit()

        #INSERT INTO `autotestTable` (`id`, `testCol`, `testIntCol`) VALUES (NULL, 'kbmbmnbmnmnb', '3')
        #print('addData')

    def updData(self, tableName, data, pk):

        query = 'UPDATE `'+tableName+'` SET '

        first = True

        for dataKey in data:
            if not first:
                query += ', '
            first = False
            if type(data[dataKey]) == bool:
                query += '`'+dataKey+"` = '" + str(1 if data[dataKey] else 0) + "' "
            else:
                query += '`'+dataKey+"` = '" + pymysql.escape_string(str(data[dataKey])) + "' "

        query += ' WHERE `'+tableName+'`.`'+pk+'` = ' + str(data[pk])

        #print(query)
        self.cursor.execute(query)
        self.conn.commit()

        #UPDATE `autotestTable` SET `testCol` = 'kbmbmnbfff' WHERE `autotestTable`.`id` = 1
        #print('upd')

    def getAllRows(self, tableName):
        if not self.tableExist(tableName):
            return None

        query = 'SELECT * FROM `'+tableName+'`'

        return self.exec(query)

    def getRowsWhere(self, tableName, logic):
        if not self.tableExist(tableName):
            return None

        query = 'SELECT * FROM `'+tableName+'` WHERE ' + str(logic)

        return self.exec(query)

    def deleteRow(self, tableName, pk, pkValue):
        if not self.tableExist(tableName):
            return

        query = 'DELETE FROM `'+tableName+'` WHERE `'+tableName+'`.`'+pk+'` = '+str(pkValue);

        #print(query)
        self.cursor.execute(query)
        self.conn.commit()

        #"DELETE FROM `autotestTable` WHERE `autotestTable`.`ololid` = 1"

    def exec(self, sql):
        #print(sql)
        self.cursor.execute(sql)
        self.conn.commit()
        ret = self.cursor.fetchall()

        header = []

        for descTuple in self.cursor.description:
            header.append(descTuple[0]);

        return (header, ret)

    def buildHeaderAndData(self, header, data):
        buildedRow = {}
        for idx, columnName in enumerate(header):
            buildedRow[columnName] = data[idx]
        return buildedRow

#if __name__ == '__main__':

    #db = MySQLDatabase('db4free.net', 'nyry64m2', 'Y8rGrWfE', 'video_poster')

    ##if not db.tableExist('autotestTable'):
    #    #db.createTable('autotestTable', {'testColumn' : str, 'intTestColumn' : int}, pk='ololid')

    #db.addData('autotestTable', {'testCol': 'forView', 'testIntCol': 5}, pk='ololid')

    #rows = db.getAllrows('autotestTable')

    #buildedRow = db.buildHeaderAndData(rows[0], rows[1][0])

    #buildedRow['testCol'] = 'ChangedWithUpdate'

    #db.updData('autotestTable', buildedRow, pk='ololid')

    #db.deleteRow('autotestTable', 'ololid', rows[1][len(rows[1])-3][0])

    #all = db.exec('Select * from autotestTable')

    #print (all)




